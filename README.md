# Try Catch #
 
 The try block contains set of statements where an exception can occur. A try block is always followed by a catch block, which handles the exception that occurs in associated try block.
 
 https://beginnersbook.com/2013/04/try-catch-in-java/